
#include <cstdlib>
#include <stdio.h>
#include <cassert>

#include "option.cpp"

Option<int> findSomething() {
  return Some(129312);
}

Option<int> findNothing() {
  return None<int>();
}

int main() {
  printf("Testing `optional` library\n");

  auto result = findSomething();
  assert(result.isSome() == true);
  assert(result.isNone() == false);

  auto noResult = findNothing();
  assert(noResult.isSome() == false);
  assert(noResult.isNone() == true);

  return 0;
}

