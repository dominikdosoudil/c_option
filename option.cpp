#include <cstdlib>

using namespace std;

template <class T> struct Option {
    Option(T item): item(item), some(true) {}
    Option(): some(false) {}
    bool isSome() {
      return some;
    }
    bool isNone() {
      return !isSome();
    }

    T item;
  private:
    bool some;
};

template<class T> Option<T> Some(const T& item) {
  return Option<T>(item);
}

template<class T> Option<T> None() {
  return Option<T>();
}

